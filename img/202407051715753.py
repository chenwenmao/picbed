# -*- encoding: utf-8 -*-
"""
@File    :   LSTMTime0905.py
@Time    :   2022/10/14 14:05:27
@Author  :   wenmao Chen 
"""

# 导入相关包
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import GridSearchCV
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, LSTM, Dropout
from sklearn.preprocessing import MinMaxScaler
from tensorflow.keras.wrappers.scikit_learn import KerasRegressor
from sklearn.metrics import mean_absolute_error, mean_squared_error, r2_score

plt.rcParams["font.sans-serif"] = ["Simhei"]  # 解决坐标轴刻度负号乱码
plt.rcParams["axes.unicode_minus"] = False
from datetime import date, datetime, timedelta
from sklearn.model_selection import GridSearchCV
import pymysql

# 训练模型，使用 girdsearchCV 进行参数调整以找到基础模型。


# 0907 将shift_n、p_step作为变量修改
class LSTMTimePredictor:
    def __init__(
        self, df, shift_n=30, test_ratio=0.2, n_past=30, p_step=1, optimizer="adam"
    ):
        """
        df:DataFrame时间序列数据;
        test_ratio:测试比率
        n_past:预测的窗口数;
        optimizer:优化器;
        n_features:特征数;
        feature_names:特征名称;
        shirt_n:预测时长
        p_step:预测步长
        """
        self.p_step = p_step
        self.df = df[::p_step]
        self.test_ratio = test_ratio
        self.n_past = n_past
        self.shift_n = shift_n
        self.optimizer = optimizer
        self.n_features = self.df.shape[1]
        self.feature_names = self.df.columns

    def shift_date_new(self):
        """
        时间滑动
        下一段滑动函数 n 为移动参数，即为预测长度
        df4都有的数据集用于训练和测试
        df9向后预测的数据需要的未知数据集;
        """
        df1 = self.df
        n = self.shift_n
        bl_fh = df1.iloc[:, 1 : self.n_features]
        cb = df1.iloc[:, 0]
        cb.to_frame
        df2 = cb.shift(periods=-n, axis=0)
        df3 = pd.concat([df2, bl_fh], join="outer", axis=1)
        df9 = df3[-n:]
        df4 = df3[:-n]
        return df4, df9

    def fig_dataview(self):
        """
        查看数据概览图;
        """
        df = self.df
        columns_name = df.columns.values.tolist()
        plt.figure(figsize=(14, 6))
        plt.plot(df, label=columns_name)
        plt.grid()
        plt.legend()
        plt.show()

    def _train_test_split(self):
        """
        训练测试划分;
        """
        df = self.shift_date_new()[0]
        test_split = round(len(self.df) * self.test_ratio)  # 计算测试集中的样本数量
        df_training = df[:-test_split]
        df_testing = df[-test_split:]
        # 进行最小最大归一化
        scaler = MinMaxScaler(feature_range=(0, 1))
        df_training_scaled = scaler.fit_transform(df_training)
        df_testing_scaled = scaler.transform(df_testing)

        # 获取训练集和测试集的样本数量
        self.train_length = len(df_training_scaled)
        self.test_length = len(df_testing_scaled)

        # 获取归一化后的训练样本和测试样本
        # self.df_training_scaled = df_training_scaled
        # self.df_training_scaled = df_testing_scaled
        self.scaler = scaler
        return df_training_scaled, df_testing_scaled
        # return df_training_scaled, df_testing_scaled df_training, df_training

    def createXY(self, datasets):
        """
        生成用于LSTM输入的多元数据,例如时间窗口n_past=30,则一个样本的维度为(30,5)
        30代表时间窗口,5代表特征数量
        将数据分为x y
        n_past 我们在预测下一个目标值时将在过去查看的步骤数 粒度
        n_past使用30,意味着将使用过去的30个值
        dataX 代表目标预测值dataY前所有因子,包括预测因子30个数据
        若n_past越小,则预测的平滑度越低,越注重于短期预测,若n_past越大则越注重长期预测
        """
        dataX = []
        dataY = []
        for i in range(self.n_past, len(datasets)):
            dataX.append(datasets[i - self.n_past : i, 0 : datasets.shape[1]])
            dataY.append(datasets[i, 0])
        return np.array(dataX), np.array(dataY)

    def _build_model(
        self,
    ):
        """
        建立模型,没有训练模型
        """
        grid_model = Sequential()
        grid_model.add(
            LSTM(50, return_sequences=True, input_shape=(self.n_past, self.n_features))
        )
        grid_model.add(LSTM(50))
        grid_model.add(Dropout(0.2))
        grid_model.add(Dense(1))
        grid_model.compile(loss="mse", optimizer=self.optimizer)
        # 封装为scikit-learn模型
        return grid_model

    def grid_search(
        self,
    ):
        """
        根据数据训练模型，并查找最优的参数
        """
        df_training_scaled = self._train_test_split()[0]
        df_testing_scaled = self._train_test_split()[1]
        X_train, y_train = self.createXY(df_training_scaled)
        X_test, y_test = self.createXY(df_testing_scaled)

        grid_model = KerasRegressor(
            build_fn=self._build_model, verbose=1, validation_data=(X_test, y_test)
        )
        parameters = {
            "batch_size": [16, 20],
            "epochs": [8, 10],
            # , "Adadelta"            "optimizer": ["adam"],
        }
        # 这里前文设置了optimizer为adam

        # parameters = {'batch_size' : [16,20],
        #               'epochs' : [8,10],
        #               'optimizer' : ['adam','Adadelta'] }

        grid_search = GridSearchCV(estimator=grid_model, param_grid=parameters, cv=2)
        # grid_search = grid_search.fit(X_train, y_train)
        grid_search = grid_search.fit(
            X_train, y_train, validation_data=(X_test, y_test)
        )
        self.model = grid_search.best_estimator_.model
        # self.model = grid_model

    def fit(
        self,
    ):
        """
        直接建立模型，未查找最优参数
        """
        df_training_scaled = self._train_test_split()[0]
        df_testing_scaled = self._train_test_split()[1]
        X_train, y_train = self.createXY(df_training_scaled)
        X_test, y_test = self.createXY(df_testing_scaled)
        grid_model = KerasRegressor(
            build_fn=self._build_model, verbose=1, validation_data=(X_test, y_test)
        )
        grid_model.fit(X_train, y_train)
        self.model = grid_model

    def eva_luate(self, plot=True):
        """
        旧的不用
        """
        df_testing_scaled = self._train_test_split()[1]
        X_test, y_test = self.createXY(df_testing_scaled)
        # 预测值
        prediction = self.model.predict(X_test)
        prediction_copy_array = np.repeat(prediction, self.n_features, axis=-1)
        pred = self.scaler.inverse_transform(
            np.reshape(prediction_copy_array, (len(prediction), self.n_features))
        )[:, 0]
        # 实际值
        original_copies_array = np.repeat(y_test, self.n_features, axis=-1)
        original = self.scaler.inverse_transform(
            np.reshape(original_copies_array, (len(y_test), self.n_features))
        )[:, 0]
        if plot:
            fig, ax = plt.subplots(figsize=(20, 8))
            ax.plot(original, color="red", label="Real Values")
            ax.plot(pred, color="blue", label="Predicted Values")
            ax.set_title("Time Series Prediction")
            ax.set_xlabel("Time")
            ax.set_ylabel("Values")
            ax.legend()
            plt.show()
        mae = mean_absolute_error(original, pred)
        mse = mean_squared_error(original, pred)
        mape = np.mean(np.abs(original - pred) / original)
        r2 = r2_score(original, pred)
        print("MSE is {},MAE is {}, MAPE is {}, r2 is {}".format(mse, mae, mape, r2))
        return pred

    def evaluate(self, plot=True):
        """
        制图及模型评价
        """
        df_testing_scaled = self._train_test_split()[1]
        X_test, y_test = self.createXY(df_testing_scaled)
        # 预测值
        prediction = self.model.predict(X_test)
        prediction_copy_array = np.repeat(prediction, self.n_features, axis=-1)
        pred = self.scaler.inverse_transform(
            np.reshape(prediction_copy_array, (len(prediction), self.n_features))
        )[:, 0]
        # 实际值
        original_copies_array = np.repeat(y_test, self.n_features, axis=-1)
        original = self.scaler.inverse_transform(
            np.reshape(original_copies_array, (len(y_test), self.n_features))
        )[:, 0]
        # 序号还原

        df = self.shift_date_new()[0]
        test_split = round(len(self.df) * self.test_ratio)  # 计算测试集中的样本数量
        df_training = df[:-test_split]
        df_testing = df[-test_split:]

        index1 = df_testing.index
        starttime = index1[self.n_past]
        delta = timedelta(hours=self.n_past)  # 时间序号还原
        starttime = starttime + delta
        starttime = starttime.strftime("%Y-%m-%d %H:%M:%S")
        endtime = index1[-1]
        endtime = endtime + delta
        endtime = endtime.strftime("%Y-%m-%d %H:%M:%S")
        time_nu = pd.date_range(starttime, endtime, freq="h").strftime(
            "%Y-%m-%d %H:%M:%S"
        )
        time_nu = time_nu[:: self.p_step]

        original_2 = pd.DataFrame(original)
        col_2 = ["真实值"]
        original_2.columns = col_2
        original_3 = original_2.set_index(time_nu)

        pred_2 = pd.DataFrame(pred)
        col_1 = ["预测值"]
        pred_2.columns = col_1
        pred_3 = pred_2.set_index(time_nu)
        if plot:
            plt.figure(figsize=(14, 6))
            plt.plot(original_3, color="red", label="真实值")
            plt.plot(pred_3, color="blue", label="预测值")
            # plt.title(" 茨坝氨氮预测")
            plt.xlabel("Time")
            plt.ylabel(" 预测值")
            plt.locator_params(axis="x", nbins=10)
            plt.xticks(range(1, len(time_nu), 48), rotation=45)  # 刻度线显示优化
            plt.legend()
            plt.show()

        mae = mean_absolute_error(original, pred)
        mse = mean_squared_error(original, pred)
        mape = np.mean(np.abs(original - pred) / original)
        r2 = r2_score(original, pred)
        acc = 1 - abs((pred - original) / original)
        acc = np.mean(acc)
        print(
            "MSE : {:.4f},MAE : {:.4f}, MAPE : {:.3%}, r2 : {:.2f}, 准确率:{:.3%}".format(
                mse, mae, mape, r2, acc
            )
        )

        # print(original_3)
        # print(pred_3)
        return pred

    def predict(self):
        df1 = self.shift_date_new()[0]
        n = self.shift_n
        df_past = df1.iloc[-n:, :]
        df_future = self.shift_date_new()[1]

        old_scaled_array = self.scaler.transform(df_past)
        new_scaled_array = self.scaler.transform(df_future)
        new_scaled_df = pd.DataFrame(new_scaled_array)
        full_df = (
            pd.concat([pd.DataFrame(old_scaled_array), new_scaled_df])
            .reset_index()
            .drop(["index"], axis=1)
        )

        full_df_scaled_array = full_df.values
        all_data = []
        time_step = self.shift_n
        for i in range(time_step, len(full_df_scaled_array)):
            data_x = []
            data_x.append(
                full_df_scaled_array[
                    i - time_step : i, 0 : full_df_scaled_array.shape[1]
                ]
            )
            data_x = np.array(data_x)
            prediction = self.model.predict(data_x)
            all_data.append(prediction)
            full_df.iloc[i, 0] = prediction

        new_array = np.array(all_data)
        new_array = new_array.reshape(-1, 1)
        prediction_copies_array = np.repeat(new_array, self.n_features, axis=-1)
        y_pred_future_days = self.scaler.inverse_transform(
            np.reshape(prediction_copies_array, (len(new_array), self.n_features))
        )[:, 0]

        index9 = df_future.index
        future_value = pd.DataFrame(y_pred_future_days)
        future_value = future_value.set_index(index9)
        col_NM = df1.columns[0]
        future_value.columns = [col_NM]
        frep = str(self.n_past) + "h"
        future_value = future_value.shift(self.p_step, freq=frep)
        return future_value

    def fig_predict(self):
        """
        查看数据概览图;
        """
        df = self.predict()
        plt.figure(figsize=(14, 6))
        plt.plot(df, color="red", label="预测值")
        plt.xlabel("Time")
        plt.ylabel(" 预测值")
        plt.title("水质预测")
        plt.locator_params(axis="x", nbins=10)
        # plt.xticks(range(1,len(time_nu),48),rotation=45) # 刻度线显示优化
        plt.legend()
        plt.show()
